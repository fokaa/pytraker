"""Tracking application for GPS enabled devices

"""
from pytraker.trakerserver import TrakerServer

server = TrakerServer()

if __name__ == "__main__":
    # loglevel passed to run method will overwrite setting from config file
    server.run(loglevel="debug")

