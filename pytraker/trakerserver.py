""" Main traker server class 


"""
from .config import readConfig

class TrakerServer():
    database = None
    api = None
    devices = None
    users = None
    config = None

    """Use loglevel if provided
    
    """
    def __init__(self, loglevel=None):
        __class__.config = readConfig()

    """Run server for debugging
    
    """
    def run(self, loglevel=None):
        if loglevel:
            __class__.config['loglevel'] = loglevel
        print("Server running")