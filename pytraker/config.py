"""read config file for pytraker


"""
import configparser
import os
import sys

def readConfig(confpath='./pytraker.cfg'):
    
    config = {
            "db": {
                "type": "",
                "database": "",
                "login": "",
                "password": ""
            },
            "loglevel": "info"        
    }
    
    if not os.path.exists(confpath):
        print(f"pytraker: config file {confpath} does not exist \n")
        sys.exit(1)
    
    try:
        confile = configparser.ConfigParser()
        confile.read(confpath)
        config["loglevel"] =  confile.get("Server", "loglevel")
        config["db"]["type"] = confile.get("DB", "type")
        config["db"]["database"] = confile.get("DB", "database")
        if confile.has_option("DB", "login"):
            config["db"]["login"] = confile.get("DB", "login")
        if confile.has_option("DB", "password"):
            config["db"]["password"] = confile.get("DB", "password")
    except Exception as e:
        if (e.section):
            print(f"pytraker: error reading config file {confpath} section {e.section} \n")
        else:
            print(f"pytraker: error reading config file {confpath} \n")
        sys.exit(1)

    return config